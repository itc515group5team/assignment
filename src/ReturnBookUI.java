import java.util.Scanner;


public class ReturnBookUI {

	public static enum UI_STATE { INITIALISED, READY, INSPECTING, COMPLETED };

	private ReturnBookControl returnBookControl;
	private Scanner input;
	private UI_STATE userInterfaceState;


	public ReturnBookUI(ReturnBookControl returnBookControl) {
		this.returnBookControl = returnBookControl;
		input = new Scanner(System.in);
		userInterfaceState = UI_STATE.INITIALISED;
		returnBookControl.setUI(this);
	}


	public void run() {
		output("Return Book Use Case UI\n");

		while (true) {

		switch (userInterfaceState) {

			case INITIALISED:
			break;

			case READY:
				String bookScan = input("Scan Book (<enter> completes): ");
				if (bookScan.length() == 0) {
					returnBookControl.scanningComplete();
				}
				else {
					try {
						int bookId = Integer.valueOf(bookScan).intValue();
						returnBookControl.bookScanned(bookId);
					}
					catch (NumberFormatException e) {
						output("Invalid bookId");
					}
				}
			break;

			case INSPECTING:
				String bookStatus = input("Is book damaged? (Y/N): ");
				boolean isDamaged = false;
				if (bookStatus.toUpperCase().equals("Y")) {
					isDamaged = true;
				}
				returnBookControl.dischargeLoan(isDamaged);

			case COMPLETED:
				output("Return processing complete");
			return;

			default:
				output("Unhandled state");
				throw new RuntimeException("ReturnBookUI : unhandled state :" + userInterfaceState);
			}
		}
	}


	private String input(String prompt) {
		System.out.print(prompt);
		return input.nextLine();
	}


	private void output(Object object) {
		System.out.println(object);
	}


	public void display(Object object) {
		output(object);
	}

	
	public void setState(UI_STATE userInterfaceState) {
		this.userInterfaceState = userInterfaceState;
	}


}
