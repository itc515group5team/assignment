import java.io.Serializable;


@SuppressWarnings("serial")
public class Book implements Serializable {

    private String title;
    private String author;
    private String callNo;
    private int ID;

    private enum STATE {AVAILABLE, ON_LOAN, DAMAGED, RESERVED};

    private STATE state;


    public Book(String author, String title, String callNo, int ID) {
        this.author = author;
        this.title = title;
        this.callNo = callNo;
        this.ID = ID;
        this.state = STATE.AVAILABLE;
    }


    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Book: ").append(ID).append("\n")
                .append("  Title:  ").append(title).append("\n")
                .append("  Author: ").append(author).append("\n")
                .append("  CallNo: ").append(callNo).append("\n")
                .append("  State:  ").append(state);

        return stringBuilder.toString();
    }


    public Integer ID() {
        return ID;
    }


    public String getTitle() {
        return title;
    }


    public boolean isAvailable() {
        return state == STATE.AVAILABLE;
    }


    public boolean onLoan() {
        return state == STATE.ON_LOAN;
    }


    public boolean isDamaged() {
        return state == STATE.DAMAGED;
    }


    public void borrowBook() {
        if (state.equals(STATE.AVAILABLE)) {
            state = STATE.ON_LOAN;
        } else {
            throw new RuntimeException(String.format("Book: cannot borrow while book is in state: %s", state));
        }

    }


    public void returnBook(boolean DAMAGED) {
        if (state.equals(STATE.ON_LOAN)) {
            if (DAMAGED) {
                state = STATE.DAMAGED;
            } else {
                state = STATE.AVAILABLE;
            }
        } else {
            throw new RuntimeException(String.format("Book: cannot Return while book is in state: %s", state));
        }
    }


    public void repairBook() {
        if (state.equals(STATE.DAMAGED)) {
            state = STATE.AVAILABLE;
        } else {
            throw new RuntimeException(String.format("Book: cannot repair while book is in state: %s", state));
        }
    }
}
