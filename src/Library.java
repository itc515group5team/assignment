import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class Library implements Serializable {

    private static final String LIBRARY_FILE = "library.obj";
    private static final int LOAN_LIMIT = 2;
    private static final int LOAN_PERIOD = 2;
    private static final double FINE_PER_DAY = 1.0;
    private static final double MAX_FINES_OWED = 5.0;
    private static final double DAMAGE_FEE = 2.0;

    private static Library self;
    private int bookID;
    private int memberID;
    private int libraryID;
    private Date loadDate;

    private Map<Integer, Book> catalog;
    private Map<Integer, Member> members;
    private Map<Integer, Loan> loans;
    private Map<Integer, Loan> currentLoans;
    private Map<Integer, Book> damagedBooks;

    private Library() {
        catalog = new HashMap<>();
        members = new HashMap<>();
        loans = new HashMap<>();
        currentLoans = new HashMap<>();
        damagedBooks = new HashMap<>();
        bookID = 1;
        memberID = 1;
        libraryID = 1;
    }

    public static synchronized Library getInstance() {
        if (self == null) {
            Path path = Paths.get(LIBRARY_FILE);
            if (Files.exists(path)) {
                try (ObjectInputStream lof = new ObjectInputStream(new FileInputStream(LIBRARY_FILE))) {
                    self = (Library) lof.readObject();
                    Calendar.getInstance().setDate(self.loadDate);
                    lof.close();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            } else self = new Library();
        }
        return self;
    }

    public static synchronized void save() {
        if (self != null) {
            self.loadDate = Calendar.getInstance().Date();
            try (ObjectOutputStream lof = new ObjectOutputStream(new FileOutputStream(LIBRARY_FILE));) {
                lof.writeObject(self);
                lof.flush();
                lof.close();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public int getBookID() {
        return bookID;
    }

    public int getMemberID() {
        return memberID;
    }

    private int getNextBookID() {
        return bookID++;
    }

    private int getNextMemberID() {
        return memberID++;
    }

    private int getNextLibraryID() {
        return libraryID++;
    }

    public List<Member> getListOfMember() {
        return new ArrayList<Member>(members.values());
    }

    public List<Book> getListOfBooks() {
        return new ArrayList<Book>(catalog.values());
    }

    public List<Loan> getListOfCurrentLoans() {
        return new ArrayList<Loan>(currentLoans.values());
    }

    public Member addNewMember(String lastName, String firstName, String email, int phoneNo) {
        Member member = new Member(lastName, firstName, email, phoneNo, getNextMemberID());
        members.put(member.getID(), member);
        return member;
    }

    public Book addNewBook(String author, String title, String callNo) {
        Book book = new Book(author, title, callNo, getNextBookID());
        catalog.put(book.ID(), book);
        return book;
    }

    public Member getMember(int memberID) {
        if (members.containsKey(memberID)) {
            return members.get(memberID);
        }
        return null;
    }

    public Book book(int bookID) {
        if (catalog.containsKey(bookID)) {
            return catalog.get(bookID);
        }
        return null;
    }

    public int loanLimit() {
        return LOAN_LIMIT;
    }

    public boolean memberCanBorrow(Member member) {
        if (member.getNumberOfCurrentLoans() == LOAN_LIMIT) {
            return false;
        }

        if (member.getFinesOwed() >= MAX_FINES_OWED) {
            return false;
        }

        for (Loan loan : member.getLoans()) {
            if (loan.isOverDue()) {
                return false;
            }
        }
        return true;
    }

    public int loansRemainingForMember(Member member) {
        return LOAN_LIMIT - member.getNumberOfCurrentLoans();
    }

    public Loan issueLoan(Book book, Member member) {
        Date dueDate = Calendar.getInstance().getDueDate(LOAN_PERIOD);
        Loan loan = new Loan(getNextLibraryID(), book, member, dueDate);
        member.takeOutLoan(loan);
        book.borrowBook();
        loans.put(loan.getID(), loan);
        currentLoans.put(book.ID(), loan);
        return loan;
    }

    public Loan getLoanByBookID(int bookID) {
        if (currentLoans.containsKey(bookID)) {
            return currentLoans.get(bookID);
        }
        return null;
    }

    public double calculateOverDueFine(Loan loan) {
        if (loan.isOverDue()) {
            long daysOverDue = Calendar.getInstance().getDaysDifference(loan.getDueDate());
            double fine = daysOverDue * FINE_PER_DAY;
            return fine;
        }
        return 0.0;
    }

    public void dischargeLoan(Loan currentLoan, boolean isDamaged) {
        Member member = currentLoan.getMember();
        Book book = currentLoan.getBook();

        double overDueFine = calculateOverDueFine(currentLoan);
        member.addFine(overDueFine);

        member.dischargeLoan(currentLoan);
        book.returnBook(isDamaged);
        if (isDamaged) {
            member.addFine(DAMAGE_FEE);
            damagedBooks.put(book.ID(), book);
        }
        currentLoan.getLoan();
        currentLoans.remove(book.ID());
    }

    public void checkCurrentLoans() {
        for (Loan loan : currentLoans.values()) {
            loan.checkOverDue();
        }
    }

    public void repairBook(Book currentBook) {
        if (damagedBooks.containsKey(currentBook.ID())) {
            currentBook.repairBook();
            damagedBooks.remove(currentBook.ID());
        } else {
            throw new RuntimeException("Library: repairBook: book is not damaged");
        }
    }

}
