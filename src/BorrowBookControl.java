import java.util.ArrayList;
import java.util.List;

public class BorrowBookControl {
	
	private BorrowBookUI userInterface;
	
	private Library libraryObject;
	private Member memberObject;
	private enum CONTROL_STATE { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };
	private CONTROL_STATE state;
	
	private List<Book> PENDING;
	private List<Loan> COMPLETED;
	private Book bookObject;
	
	
	public BorrowBookControl() {
		this.libraryObject = libraryObject.getInstance();
		state = CONTROL_STATE.INITIALISED;
	}
	

	public void setUserInterface(BorrowBookUI userInterface) {
		if (!state.equals(CONTROL_STATE.INITIALISED)) {
			throw new RuntimeException("BorrowBookControl: cannot call setUI except in INITIALISED state");
		}
			
		this.userInterface = userInterface;
        userInterface.setState(BorrowBookUI.UI_STATE.READY);
		state = CONTROL_STATE.READY;		
	}

		
	public void cardSwiped(int memberID) {
		if (!state.equals(CONTROL_STATE.READY)) {
			throw new RuntimeException("BorrowBookControl: cannot call cardSwiped except in READY state");
		}
			
		memberObject = libraryObject.getMember(memberID);
		if (memberObject == null) {
            userInterface.display("Invalid memberId");
			return;
		}

		if (libraryObject.memberCanBorrow(memberObject)) {
			PENDING = new ArrayList<>();
            userInterface.setState(BorrowBookUI.UI_STATE.SCANNING);
			state = CONTROL_STATE.SCANNING;
		}
		else {
            userInterface.display("Member cannot borrow at this time");
            userInterface.setState(BorrowBookUI.UI_STATE.RESTRICTED);
		}
	}
	
	
	public void bookScanned(int bookID) {
		bookObject = null;
		if (!state.equals(CONTROL_STATE.SCANNING)) {
			throw new RuntimeException("BorrowBookControl: cannot call bookScanned except in SCANNING state");
		}

		bookObject = libraryObject.book(bookID);

		if (bookObject == null) {
			userInterface.display("Invalid bookId");
			return;
		}

		if (!bookObject.isAvailable()) {
			userInterface.display("Book cannot be borrowed");
			return;
		}

		PENDING.add(bookObject);
		for (Book book : PENDING) {
			userInterface.display(bookObject.toString());
		}

		if (libraryObject.loansRemainingForMember(memberObject) - PENDING.size() == 0) {
			userInterface.display("Loan limit reached");
			borrowCompleted();
		}
	}
	
	
	public void borrowCompleted() {
		if (PENDING.size() == 0) {
            cancelBurrow();
		}
		else {
			userInterface.display("\nFinal Borrowing List");

			for (Book book : PENDING) {
				userInterface.display(book.toString());
			}

			COMPLETED = new ArrayList<Loan>();
			userInterface.setState(BorrowBookUI.UI_STATE.FINALISING);
			state = CONTROL_STATE.FINALISING;
		}
	}


	public void commitLoans() {
		if (!state.equals(CONTROL_STATE.FINALISING)) {
			throw new RuntimeException("BorrowBookControl: cannot call commitLoans except in FINALISING state");
		}

		for (Book book : PENDING) {
			Loan loan = libraryObject.issueLoan(book, memberObject);
			COMPLETED.add(loan);			
		}

		userInterface.display("Completed Loan Slip");

		for (Loan loan : COMPLETED) {
			userInterface.display(loan.toString());
		}

		userInterface.setState(BorrowBookUI.UI_STATE.COMPLETED);
		state = CONTROL_STATE.COMPLETED;
	}

	
	public void cancelBurrow() {
		userInterface.setState(BorrowBookUI.UI_STATE.CANCELLED);
		state = CONTROL_STATE.CANCELLED;
	}
}
