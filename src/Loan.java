import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("serial")
public class Loan implements Serializable {
	
	public static enum LOAN_STATE { CURRENT, OVER_DUE, DISCHARGED };
	
	private int ID;
	private Book book;
	private Member member;
	private Date dueDate;
	private LOAN_STATE state;

	
	public Loan(int loanID, Book book, Member member, Date dueDate) {
		this.ID = loanID;
		this.book = book;
		this.member = member;
		this.dueDate = dueDate;
		this.state = LOAN_STATE.CURRENT;
	}

	
	public void checkOverDue() {
		if (state == LOAN_STATE.CURRENT &&
			Calendar.getInstance().Date().after(dueDate)) {
			this.state = LOAN_STATE.OVER_DUE;			
		}
	}

	
	public boolean isOverDue() {
		return state == LOAN_STATE.OVER_DUE;
	}

	
	public int getID() {
		return ID;
	}


	public Date getDueDate() {
		return dueDate;
	}
	
	
	public String toString() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

		StringBuilder sb = new StringBuilder();
		sb.append("Loan:  ").append(ID).append("\n")
		  .append("  Borrower ").append(member.getID()).append(" : ")
		  .append(member.getLastName()).append(", ").append(member.getFirstName()).append("\n")
		  .append("  Book ").append(book.ID()).append(" : " )
		  .append(book.getTitle()).append("\n")
		  .append("  DueDate: ").append(simpleDateFormat.format(dueDate)).append("\n")
		  .append("  State: ").append(state);		
		return sb.toString();
	}


	public Member getMember() {
		return member;
	}


	public Book getBook() {
		return book;
	}


	public void getLoan() {
		state = LOAN_STATE.DISCHARGED;		
	}

}
