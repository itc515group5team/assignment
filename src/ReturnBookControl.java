public class ReturnBookControl {

	private ReturnBookUI returnBookUi;
	private enum CONTROL_STATE { INITIALISED, READY, INSPECTING };
	private CONTROL_STATE controlState;
	
	private Library library;
	private Loan currentLoan;
	

	public ReturnBookControl() {
		this.library = library.getInstance();
		controlState = CONTROL_STATE.INITIALISED;
	}
	
	
	public void setUI(ReturnBookUI returnBookUi) {
		if (!controlState.equals(CONTROL_STATE.INITIALISED)) {
			throw new RuntimeException("ReturnBookControl: cannot call setUI except in INITIALISED state");
		}	
		this.returnBookUi = returnBookUi;
		returnBookUi.setState(ReturnBookUI.UI_STATE.READY);
		controlState = CONTROL_STATE.READY;
	}


	public void bookScanned(int bookId) {
		if (!controlState.equals(CONTROL_STATE.READY)) {
			throw new RuntimeException("ReturnBookControl: cannot call bookScanned except in READY state");
		}	
		Book currentBook = library.book(bookId);
		
		if (currentBook == null) {
			returnBookUi.display("Invalid Book Id");
			return;
		}
		if (!currentBook.onLoan()) {
			returnBookUi.display("Book has not been borrowed");
			return;
		}		
		currentLoan = library.getLoanByBookID(bookId);
		double overDueFine = 0.0;
		if (currentLoan.isOverDue()) {
			overDueFine = library.calculateOverDueFine(currentLoan);
		}
		returnBookUi.display("Inspecting");
		returnBookUi.display(currentBook.toString());
		returnBookUi.display(currentLoan.toString());
		
		if (currentLoan.isOverDue()) {
			returnBookUi.display(String.format("\nOverdue fine : $%.2f", overDueFine));
		}
		returnBookUi.setState(ReturnBookUI.UI_STATE.INSPECTING);
		controlState = CONTROL_STATE.INSPECTING;
	}


	public void scanningComplete() {
		if (!controlState.equals(CONTROL_STATE.READY)) {
			throw new RuntimeException("ReturnBookControl: cannot call scanningComplete except in READY state");
		}
		returnBookUi.setState(ReturnBookUI.UI_STATE.COMPLETED);
	}


	public void dischargeLoan(boolean isDamaged) {
		if (!controlState.equals(CONTROL_STATE.INSPECTING)) {
			throw new RuntimeException("ReturnBookControl: cannot call dischargeLoan except in INSPECTING state");
		}	
		library.dischargeLoan(currentLoan, isDamaged);
		currentLoan = null;
		returnBookUi.setState(ReturnBookUI.UI_STATE.READY);
		controlState = CONTROL_STATE.READY;
	}


}
