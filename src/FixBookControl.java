public class FixBookControl {
	
	private FixBookUI fixBookUI;
	private enum CONTROL_STATE { INITIALISED, READY, FIXING }
	private CONTROL_STATE controlState;
	private Library library;
	private Book currentBook;

	public FixBookControl() {
		this.library = library.getInstance();
		controlState = CONTROL_STATE.INITIALISED;
	}
	
	public void setUI(FixBookUI fixBookUI) {
		if (!controlState.equals(CONTROL_STATE.INITIALISED)) {
			throw new RuntimeException("FixBookControl: cannot call setUI except in INITIALISED state");
		}
		this.fixBookUI = fixBookUI;
		fixBookUI.setState(FixBookUI.UI_STATE.READY);
		controlState = CONTROL_STATE.READY;
	}

	public void bookScanned(int bookId) {
		if (!controlState.equals(CONTROL_STATE.READY)) {
			throw new RuntimeException("FixBookControl: cannot call bookScanned except in READY state");
		}

		currentBook = library.book(bookId);
		
		if (currentBook == null) {
			fixBookUI.display("Invalid bookId");
			return;
		}

		if (!currentBook.isDamaged()) {
			fixBookUI.display("\"Book has not been damaged");
			return;
		}

		fixBookUI.display(currentBook.toString());
		fixBookUI.setState(FixBookUI.UI_STATE.FIXING);
		controlState = CONTROL_STATE.FIXING;
	}

	public void fixBook(boolean fix) {
		if (!controlState.equals(CONTROL_STATE.FIXING)) {
			throw new RuntimeException("FixBookControl: cannot call fixBook except in FIXING state");
		}

		if (fix) {
			library.repairBook(currentBook);
		}

		currentBook = null;
		fixBookUI.setState(FixBookUI.UI_STATE.READY);
		controlState = CONTROL_STATE.READY;
	}
	
	public void scanningComplete() {
		if (!controlState.equals(CONTROL_STATE.READY)) {
			throw new RuntimeException("FixBookControl: cannot call scanningComplete except in READY state");
		}
		fixBookUI.setState(FixBookUI.UI_STATE.COMPLETED);
	}

}
