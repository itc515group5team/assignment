import java.util.Scanner;


public class FixBookUI {

	public static enum UI_STATE { INITIALISED, READY, FIXING, COMPLETED };

	private FixBookControl fixBookControl;
	private Scanner input;
	private UI_STATE userInterfaceState;

	
	public FixBookUI(FixBookControl fixBookControl) {
		this.fixBookControl = fixBookControl;
		input = new Scanner(System.in);
		userInterfaceState = UI_STATE.INITIALISED;
		fixBookControl.setUI(this);
	}


	public void setState(UI_STATE userInterfaceState) {
		this.userInterfaceState = userInterfaceState;
	}

	
	public void run() {
		output("Fix Book Use Case UI\n");
		
		while (true) {
			
			switch (userInterfaceState) {
			
			case READY:
				String bookScan = input("Scan Book (<enter> completes): ");
				if (bookScan.length() == 0) {
					fixBookControl.scanningComplete();
				}
				else {
					try {
						int bookId = Integer.valueOf(bookScan).intValue();
						fixBookControl.bookScanned(bookId);
					}
					catch (NumberFormatException e) {
						output("Invalid bookId");
					}
				}
				break;	
				
			case FIXING:
				String checkFixBook = input("Fix Book? (Y/N) : ");
				boolean isFixed = false;
				if (checkFixBook.toUpperCase().equals("Y")) {
					isFixed = true;
				}
				fixBookControl.fixBook(isFixed);
				break;
								
			case COMPLETED:
				output("Fixing process complete");
				return;
			
			default:
				output("Unhandled state");
				throw new RuntimeException("FixBookUI : unhandled state :" + userInterfaceState);
			
			}		
		}
		
	}

	
	private String input(String prompt) {
		System.out.print(prompt);
		return input.nextLine();
	}	
		
		
	private void output(Object object) {
		System.out.println(object);
	}
	

	public void display(Object object) {
		output(object);
	}
	
	
}
