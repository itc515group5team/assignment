import java.util.Scanner;

public class PayFineUI {

	public static enum UI_STATE { INITIALISED, READY, PAYING, COMPLETED, CANCELLED }
	private PayFineControl payFineControl;
	private Scanner userInput;
	private UI_STATE stateUI;
	
	public PayFineUI(PayFineControl control) {
		this.payFineControl = control;
		userInput = new Scanner(System.in);
		stateUI = UI_STATE.INITIALISED;
		control.setUI(this);
	}
	
	public void setState(UI_STATE stateUI) {
		this.stateUI = stateUI;
	}

	public void run() {
		output("Pay Fine Use Case UI\n");
		
		while (true) {
			switch (stateUI) {
			case READY:
				String messageToUser = input("Swipe member card (press <enter> to cancel): ");
				if (messageToUser.length() == 0) {
					payFineControl.cancel();
					break;
				}
				try {
					int memberId = Integer.valueOf(messageToUser).intValue();
					payFineControl.cardSwiped(memberId);
				}
				catch (NumberFormatException e) {
					output("Invalid memberId");
				}
				break;
			case PAYING:
				double amount = 0;
				String messageToUserAmount = input("Enter amount (<Enter> cancels) : ");
				if (messageToUserAmount.length() == 0) {
					payFineControl.cancel();
					break;
				}
				try {
					amount = Double.valueOf(messageToUserAmount).doubleValue();
				}
				catch (NumberFormatException e) {}
				if (amount <= 0) {
					output("Amount must be positive");
					break;
				}
				payFineControl.payFine(amount);
				break;
			case CANCELLED:
				output("Pay Fine process cancelled");
				return;
			case COMPLETED:
				output("Pay Fine process complete");
				return;
			default:
				output("Unhandled state");
				throw new RuntimeException("FixBookUI : unhandled state :" + stateUI);
			}
		}
	}
	
	private String input(String prompt) {
		System.out.print(prompt);
		return userInput.nextLine();
	}
		
	private void output(Object object) {
		System.out.println(object);
	}

	public void display(Object object) {
		output(object);
	}

}
